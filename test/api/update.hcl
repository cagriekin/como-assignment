request update {
  url = "${env.url}/customer/${request.get.body.id}"
  method = "put"

  headers = {
    content-type = "application/json"
    x-api-key = env.apiKey
  }

  body = {
    firstName = gofakeitFirstName()
    lastName = gofakeitLastName()
    birthDate = "${gofakeitYear()}-${format("%02.f", gofakeitNumber(1, 12))}-${format("%02.f", gofakeitDay())}"
  }
}

test update {
  correct-status = assertEqual(204, request.update.status)
}
