request get {
  url = "${env.url}/customer/${request.create.body.id}"
  method = "GET"

  headers = {
    x-api-key = env.apiKey
  }
}

test get {
  id = assertUUIDv4(request.get.body.id)
  firstName = assertEqual(var.first-name, request.get.body.firstName)
  lastName = assertEqual(var.last-name, request.get.body.lastName)
  age = assertGreaterThan(request.get.body.age, 0)
}
