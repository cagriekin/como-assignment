request list {
  url = "${env.url}/customer"
  method = "GET"

  headers = {
    x-api-key = env.apiKey
  }
}

test list {
  has-customers = assertGreaterThan(length(request.list.body), 0)
}

request list-order-lastname-desc {
  url = "${env.url}/customer?order=lastName&dir=desc&limit=10&page=1"
  method = "GET"

  headers = {
    x-api-key = env.apiKey
  }
}
