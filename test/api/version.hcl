request version {
  url = "${env.url}/version"
  method = "GET"
}

test version {
  got-name = assertEqual("como-assignment", request.version.body.name)
  got-version = assertRegex(
    "^([0-9]+)\\.([0-9]+)\\.([0-9]+)(?:-([0-9A-Za-z-]+(?:\\.[0-9A-Za-z-]+)*))?(?:\\+[0-9A-Za-z-]+)?$",
    request.version.body.version
  )
}
