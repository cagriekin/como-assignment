dynamic first-name {
  value = gofakeitFirstName()
}

dynamic last-name {
  value = gofakeitLastName()
}

request create {
  url = "${env.url}/customer"
  method = "POST"

  headers = {
    content-type = "application/json"
    x-api-key = env.apiKey
  }

  body = {
    firstName = var.first-name
    lastName = var.last-name
    birthDate = "${gofakeitYear()}-${format("%02.f", gofakeitNumber(1, 12))}-${format("%02.f", gofakeitDay())}"
  }
}

test create {
  id = assertUUIDv4(request.create.body.id)
  first-name = assertEqual(var.first-name, request.create.body.firstName)
  last-name = assertEqual(var.last-name, request.create.body.lastName)
  age = assertGreaterThan(request.create.body.age, 0)
}
