version = "^0.12"

env local {
  default = true

  variables = {
    url = "http://localhost:8000"
    apiKey = ""
  }
}

env dev {
  secrets from_shell_env {
    type = "env-var"
    paths = {
      apikey = "APIKEY"
    }
  }

  variables = {
    url = "https://como-demo.dev.temp.restbeast.com"
    apiKey = secret.from_shell_env.apikey
  }
}

request not-found {
  url = "${env.url}/invalid-url"
  method = "GET"
}

test not-found {
  correct-message = assertEqual(request.not-found.body.message, "Not found")
}
