request delete {
  url = "${env.url}/customer/${request.get.body.id}"
  method = "DELETE"

  headers = {
    x-api-key = env.apiKey
  }
}

test delete {
  correct-status = assertEqual(request.delete.status, 202)
}
