module.exports = {
  collectCoverage: true,
  collectCoverageFrom: ['src/**/*.js'],
  verbose: true,
  rootDir: '../../',
  roots: ['<rootDir>/src/', '<rootDir>/test/unit/'],
  setupFilesAfterEnv: ['<rootDir>/test/unit/utils/setup-file.js'],
  testMatch: ['<rootDir>/test/unit/**/*.spec.js']
};
