const tracker = require('mock-knex').getTracker();
const reqResNextFactory = require('../../../utils/reqResNextFactory');
const customerDelete = require('../../../../../src/endpoints/customer/delete');

describe('/endpoints/customer/delete', () => {
  beforeAll(() => tracker.install());

  test('success', async () => {
    tracker.once('query', query => query.response(true));

    const { req, res, next } = reqResNextFactory({
      req: { params: { id: 'id-1' } }
    });

    await customerDelete(req, res, next);

    expect(res.status).toBeCalledWith(202);
    expect(res.end).toBeCalled();
  });

  describe('errors', () => {
    test('db error ', async () => {
      const { req, res, next } = reqResNextFactory({
        req: { params: { id: 'id-1' } }
      });

      tracker.once('query', () => {
        throw new Error();
      });

      await customerDelete(req, res, next);
      expect(req.log.error).toBeCalled();
    });
  });
});
