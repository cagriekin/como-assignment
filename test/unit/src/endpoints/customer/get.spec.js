const tracker = require('mock-knex').getTracker();
const reqResNextFactory = require('../../../utils/reqResNextFactory');
const customerGet = require('../../../../../src/endpoints/customer/get');
const CustomError = require('../../../../../src/lib/error');

describe('/endpoints/customer/get', () => {
  beforeAll(() => tracker.install());

  test('success', async () => {
    const customer = { id: 'id-1' };

    tracker.once('query', query => query.response(customer));

    const { req, res, next } = reqResNextFactory({
      req: { params: { id: 'id-1' } }
    });

    await customerGet(req, res, next);

    expect(res.status).toBeCalledWith(200);
    expect(res.send).toBeCalledWith(customer);
  });

  describe('errors', () => {
    test('not-found', async () => {
      const { req, res, next } = reqResNextFactory({
        req: { params: { id: 'id-1' } }
      });

      tracker.once('query', query => query.response(undefined));

      await customerGet(req, res, next);
      expect(next).toBeCalledWith(CustomError.notFound());
    });

    test('db error ', async () => {
      const { req, res, next } = reqResNextFactory({
        req: { params: { id: 'id-1' } }
      });

      tracker.once('query', () => {
        throw new Error();
      });

      await customerGet(req, res, next);
      expect(next).toBeCalledWith(CustomError.internalError());
    });
  });
});
