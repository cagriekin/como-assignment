const tracker = require('mock-knex').getTracker();
const reqResNextFactory = require('../../../utils/reqResNextFactory');
const customerUpdate = require('../../../../../src/endpoints/customer/update');
const CustomError = require('../../../../../src/lib/error');

describe('/endpoints/customer/update', () => {
  beforeAll(() => tracker.install());

  test('success', async () => {
    const { req, res, next } = reqResNextFactory({
      req: {
        params: { id: 'id-1' },
        body: {
          firstName: 'x',
          lastName: 'y',
          birthDate: 'z'
        }
      }
    });

    tracker.once('query', query => query.response(1));

    await customerUpdate(req, res, next);

    expect(res.status).toBeCalledWith(204);
    expect(res.end).toBeCalledWith();
  });

  describe('errors', () => {
    test('not-found', async () => {
      const { req, res, next } = reqResNextFactory({
        req: {
          params: { id: 'id-1' },
          body: {
            firstName: 'x',
            lastName: 'y',
            birthDate: 'z'
          }
        }
      });

      tracker.once('query', query => query.response(0));

      await customerUpdate(req, res, next);
      expect(next).toBeCalledWith(CustomError.notFound());
    });

    test('db error ', async () => {
      const { req, res, next } = reqResNextFactory({
        req: {
          params: { id: 'id-1' },
          body: {
            firstName: 'x',
            lastName: 'y',
            birthDate: 'z'
          }
        }
      });

      tracker.once('query', () => {
        throw new Error();
      });

      await customerUpdate(req, res, next);
      expect(next).toBeCalledWith(CustomError.internalError());
    });
  });
});
