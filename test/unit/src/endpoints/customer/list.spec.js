const tracker = require('mock-knex').getTracker();
const reqResNextFactory = require('../../../utils/reqResNextFactory');
const customerList = require('../../../../../src/endpoints/customer/list');
const CustomError = require('../../../../../src/lib/error');

describe('/endpoints/customer/list', () => {
  beforeAll(() => tracker.install());

  test('success', async () => {
    const list = [
      {
        id: 'c1',
        firstName: 'x',
        lastName: 'y'
      }
    ];
    const { req, res, next } = reqResNextFactory({
      req: {}
    });
    tracker.once('query', query => query.response(list));

    await customerList(req, res, next);

    expect(res.status).toBeCalledWith(200);
    expect(res.send).toBeCalledWith(list);
  });

  describe('errors', () => {
    test('db error ', async () => {
      const { req, res, next } = reqResNextFactory({
        req: { params: { id: 'id-1' } }
      });

      tracker.once('query', () => {
        throw new Error();
      });

      await customerList(req, res, next);
      expect(next).toBeCalledWith(CustomError.internalError());
    });
  });
});
