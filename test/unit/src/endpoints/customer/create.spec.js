const tracker = require('mock-knex').getTracker();
const reqResNextFactory = require('../../../utils/reqResNextFactory');
const customerCreate = require('../../../../../src/endpoints/customer/create');
const CustomError = require('../../../../../src/lib/error');

jest.mock('got');
// eslint-disable-next-line import/order
const got = require('got');

describe('/endpoints/customer/get', () => {
  beforeEach(() => tracker.install());

  afterEach(() => tracker.uninstall());

  test('success', async () => {
    const customer = { id: 'id-1', firstName: 'x', lastName: 'y', birthDate: 'xxx' };

    tracker.on('query', query => query.response(customer));

    got.mockImplementation(() =>
      Promise.resolve({
        body: { value: { joke: `YYY, XXX makes a joke` } }
      })
    );

    const { req, res, next } = reqResNextFactory({
      req: {
        body: {
          firstName: customer.firstName,
          lastName: customer.lastName,
          birthDate: customer.birthDate
        }
      }
    });

    await customerCreate(req, res, next);

    expect(res.status).toBeCalledWith(201);
    expect(res.send).toBeCalledWith({
      birthDate: customer.birthDate,
      firstName: customer.firstName,
      id: expect.stringMatching(/\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b/),
      joke: 'y, x makes a joke',
      lastName: customer.lastName
    });
  });

  describe('errors', () => {
    test('db error ', async () => {
      tracker.on('query', query => query.reject(new Error()));

      got.mockImplementation(() =>
        Promise.resolve({
          body: { value: { joke: `YYY, XXX makes a joke` } }
        })
      );

      const { req, res, next } = reqResNextFactory({
        req: {
          body: {
            firstName: 'x',
            lastName: 'y',
            birthDate: 'z'
          }
        }
      });

      await customerCreate(req, res, next);

      expect(next).toBeCalledWith(CustomError.internalError());
    });

    test('upstream error ', async () => {
      const customer = { id: 'id-1', firstName: 'x', lastName: 'y', birthDate: 'xxx' };

      tracker.on('query', query => query.response(customer));
      got.mockImplementation(() => Promise.reject(new Error()));

      const { req, res, next } = reqResNextFactory({
        req: {
          body: {
            firstName: customer.firstName,
            lastName: customer.lastName,
            birthDate: customer.birthDate
          }
        }
      });

      await customerCreate(req, res, next);

      expect(next).toBeCalledWith(CustomError.badGateway());
    });
  });
});
