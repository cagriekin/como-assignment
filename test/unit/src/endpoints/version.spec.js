const reqResNextFactory = require('../../utils/reqResNextFactory');
const version = require('../../../../src/endpoints/version');

describe('/endpoints/version', () => {
  test('success', async () => {
    const { req, res } = reqResNextFactory();

    await version(req, res);

    expect(res.status).toBeCalledWith(200);
    expect(res.send).toBeCalledWith({
      name: expect.anything(),
      version: expect.anything()
    });
  });
});
