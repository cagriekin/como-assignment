const routeNotFound = require('../../../../src/lib/route-not-found');
const CustomError = require('../../../../src/lib/error');

describe('lib/route-not-found', () => {
  test('success', () => {
    const mock = jest.fn();
    routeNotFound(null, null, mock);

    expect(mock).toBeCalledWith(CustomError.notFound());
  });
});
