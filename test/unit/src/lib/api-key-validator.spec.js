const apiKeyValidator = require('../../../../src/lib/api-key-validator');
const reqResNextFactory = require('../../utils/reqResNextFactory');
const CustomError = require('../../../../src/lib/error');

describe('lib/api-key-validator', () => {
  test('no api key provided', () => {
    const { req, res, next } = reqResNextFactory();
    delete process.env.API_KEY;

    apiKeyValidator(req, res, next);

    expect(next).toBeCalledWith();
  });

  test('no api key given', () => {
    const { req, res, next } = reqResNextFactory();
    process.env.API_KEY = 'x';

    apiKeyValidator(req, res, next);

    expect(next).toBeCalledWith(CustomError.forbidden());
  });

  test('invalid api key given', () => {
    const { req, res, next } = reqResNextFactory({
      req: {
        get: jest.fn().mockReturnValue('x')
      }
    });
    process.env.API_KEY = 'y';

    apiKeyValidator(req, res, next);

    expect(next).toBeCalledWith(CustomError.forbidden());
  });

  test('valid api key given', () => {
    const { req, res, next } = reqResNextFactory({
      req: {
        get: jest.fn().mockReturnValue('x')
      }
    });
    process.env.API_KEY = 'x';

    apiKeyValidator(req, res, next);

    expect(next).toBeCalledWith();
  });
});
