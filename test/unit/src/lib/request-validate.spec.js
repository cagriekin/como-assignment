const requestValidator = require('../../../../src/lib/request-validator');
const reqResNextFactory = require('../../utils/reqResNextFactory');
const CustomError = require('../../../../src/lib/error');

describe('lib/request-validate', () => {
  test('success', async () => {
    const { req, res, next } = reqResNextFactory({
      req: {
        body: {
          firstName: 'x',
          lastName: 'y',
          birthDate: '1970-01-01'
        },
        get: jest.fn().mockReturnValue('application/json')
      }
    });

    const validator = await requestValidator('./openapi.yml');
    const middleware = validator('customerCreate');

    await middleware(req, res, next);

    expect(next).toHaveBeenCalledWith();
  });

  test('no body', async () => {
    const { req, res, next } = reqResNextFactory();

    const validator = await requestValidator('./openapi.yml');
    const middleware = validator('customerGet');

    await middleware(req, res, next);

    expect(next).toHaveBeenCalledWith();
  });

  test('bad request', async () => {
    const { req, res, next } = reqResNextFactory({
      req: {
        body: {
          firstName: 'x',
          lastName: 'y'
        },
        get: jest.fn().mockReturnValue(null)
      }
    });

    const validator = await requestValidator('./openapi.yml');
    const middleware = validator('customerCreate');

    await middleware(req, res, next);

    expect(next).toBeCalledWith(CustomError.badRequest({ message: '"birthDate" is required' }));
  });

  test('unknown operation id', async () => {
    const validator = await requestValidator('./openapi.yml');
    expect(() => validator('nonExistentOpId')).toThrowError(
      CustomError.notFound({ message: 'OperationId not defined in openapi spec' })
    );
  });

  test('undefined content-type', async () => {
    const { req, res, next } = reqResNextFactory({
      req: {
        body: {
          firstName: 'x',
          lastName: 'y'
        },
        get: jest.fn().mockReturnValue('plain/text')
      }
    });

    const validator = await requestValidator('./openapi.yml');
    const middleware = validator('customerCreate');

    await middleware(req, res, next);

    expect(next).toBeCalledWith(CustomError.badRequest({ message: "plain/text isn't available" }));
  });
});
