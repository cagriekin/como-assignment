const errorHandler = require('../../../../src/lib/error-handler');
const reqResNextFactory = require('../../utils/reqResNextFactory');

describe('lib/error-handler', () => {
  test('success', () => {
    const { req, res, next } = reqResNextFactory();

    const err = new Error('oh-my-error');
    err.statusCode = 400;

    errorHandler(err, req, res, next);

    expect(res.status).toBeCalledWith(400);
    expect(res.send).toBeCalledWith({ message: 'oh-my-error' });
  });

  test('default statusCode', () => {
    const { req, res, next } = reqResNextFactory();

    const err = new Error('oh-my-error');

    errorHandler(err, req, res, next);

    expect(res.status).toBeCalledWith(500);
    expect(res.send).toBeCalledWith({ message: 'oh-my-error' });
  });
});
