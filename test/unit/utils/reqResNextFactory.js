const knex = require('knex');
const mockDb = require('mock-knex');
const customerModel = require('../../../src/lib/models/customer');

/**
 * @param {?Object} override
 */
module.exports = ({ req, res, next } = {}) => {
  const db = knex({
    client: 'sqlite3',
    useNullAsDefault: true
  });
  mockDb.mock(db);
  const Customer = customerModel(db);

  const resFns = {
    json: jest.fn(),
    send: jest.fn(),
    end: jest.fn()
  };

  return {
    req: {
      body: {},
      app: {
        locals: {
          Customer
        }
      },
      params: {},
      query: {},
      log: {
        debug: jest.fn(),
        info: jest.fn(),
        warn: jest.fn(),
        error: jest.fn(),
        fatal: jest.fn()
      },
      get: () => {},
      ...req
    },
    res: {
      status: jest.fn().mockReturnValue(resFns),
      ...resFns,
      ...res
    },
    next: next || jest.fn()
  };
};
