resource "aws_secretsmanager_secret" "pass" {
  name                    = "${var.service_name}-${var.env}-db-pass"
  recovery_window_in_days = 0
}

resource "aws_secretsmanager_secret_version" "pass" {
  secret_id      = aws_secretsmanager_secret.pass.id
  secret_string  = var.db_master_pass
  version_stages = ["AWSCURRENT"]
}

resource "aws_db_subnet_group" "db" {
  name       = "db"
  subnet_ids = [aws_subnet.subnet-a.id, aws_subnet.subnet-b.id]
}

resource "aws_rds_cluster" "aurora" {
  cluster_identifier      = "${var.service_name}-${var.env}"
  engine                  = "aurora-mysql"
  engine_version          = "5.7.mysql_aurora.2.07.1"
  database_name           = replace("${var.service_name}-${var.env}", "-", "_")
  master_username         = var.env
  master_password         = var.db_master_pass
  backup_retention_period = 1
  apply_immediately       = true
  skip_final_snapshot     = true
  engine_mode             = "serverless"

  scaling_configuration {
    auto_pause     = false
    max_capacity   = 16
    min_capacity   = 1
    timeout_action = "ForceApplyCapacityChange"
  }

  db_subnet_group_name   = aws_db_subnet_group.db.name
  vpc_security_group_ids = [aws_security_group.db.id]
}
