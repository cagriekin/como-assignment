resource "aws_vpc" "priv" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_subnet" "subnet-pub" {
  cidr_block        = "10.0.2.0/24"
  vpc_id            = aws_vpc.priv.id
  availability_zone = "eu-west-1a"
}

resource "aws_subnet" "subnet-a" {
  cidr_block        = "10.0.0.0/24"
  vpc_id            = aws_vpc.priv.id
  availability_zone = "eu-west-1a"
}

resource "aws_subnet" "subnet-b" {
  cidr_block        = "10.0.1.0/24"
  vpc_id            = aws_vpc.priv.id
  availability_zone = "eu-west-1b"
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.priv.id
}

resource "aws_security_group" "egress" {
  name   = "${var.service_name}-${var.env}-egress"
  vpc_id = aws_vpc.priv.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "db" {
  name   = "${var.service_name}-${var.env}-db-sg"
  vpc_id = aws_vpc.priv.id

  ingress {
    from_port = 3306
    to_port   = 3306
    security_groups = [
    aws_security_group.egress.id]
    protocol = "tcp"
  }
}

resource "aws_eip" "nat" {

}

resource "aws_nat_gateway" "gw" {
  allocation_id = aws_eip.nat.id
  subnet_id     = aws_subnet.subnet-pub.id
}

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.priv.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }
}

resource "aws_route_table" "private" {
  vpc_id = aws_vpc.priv.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.gw.id
  }
}

resource "aws_route_table_association" "pub" {
  subnet_id      = aws_subnet.subnet-pub.id
  route_table_id = aws_route_table.public.id
}

resource "aws_route_table_association" "priv-a" {
  subnet_id      = aws_subnet.subnet-a.id
  route_table_id = aws_route_table.private.id
}

resource "aws_route_table_association" "priv-b" {
  subnet_id      = aws_subnet.subnet-b.id
  route_table_id = aws_route_table.private.id
}
