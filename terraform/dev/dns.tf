data "aws_route53_zone" "tld" {
  name = "${var.tld}."
}

resource "aws_api_gateway_domain_name" "domain" {
  depends_on               = [aws_acm_certificate_validation.cert_verify]
  domain_name              = "${var.service_name}.${var.env}.temp.${var.tld}"
  regional_certificate_arn = aws_acm_certificate.cert.arn

  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

resource "aws_api_gateway_base_path_mapping" "regional" {
  domain_name = aws_api_gateway_domain_name.domain.domain_name
  api_id      = aws_api_gateway_rest_api.rest-api.id
  stage_name  = aws_api_gateway_stage.stage.stage_name
}

resource "aws_acm_certificate" "cert" {
  domain_name               = var.tld
  subject_alternative_names = ["${var.service_name}.${var.env}.temp.${var.tld}"]
  validation_method         = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "cert_verify" {
  for_each = {
    for dvo in aws_acm_certificate.cert.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }
  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = data.aws_route53_zone.tld.zone_id
}

resource "aws_acm_certificate_validation" "cert_verify" {
  certificate_arn         = aws_acm_certificate.cert.arn
  validation_record_fqdns = [for record in aws_route53_record.cert_verify : record.fqdn]
}

resource "aws_route53_record" "a" {
  name    = "${var.service_name}.${var.env}.temp.${var.tld}"
  type    = "A"
  zone_id = data.aws_route53_zone.tld.id

  alias {
    name                   = aws_api_gateway_domain_name.domain.regional_domain_name
    zone_id                = aws_api_gateway_domain_name.domain.regional_zone_id
    evaluate_target_health = true
  }
}
