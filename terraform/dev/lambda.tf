resource "aws_lambda_function" "express" {
  function_name    = "${var.service_name}-${var.env}-fn"
  filename         = "../../dist/code.zip"
  source_code_hash = filebase64sha256("../../dist/code.zip")
  handler          = "src/lambda.handler"
  runtime          = "nodejs12.x"
  memory_size      = 1536
  timeout          = 28

  vpc_config {
    subnet_ids         = [aws_subnet.subnet-a.id, aws_subnet.subnet-b.id]
    security_group_ids = [aws_security_group.egress.id]
  }

  role = aws_iam_role.lambda-exec.arn

  environment {
    variables = {
      NODE_ENV    = "production"
      ENV         = var.env
      DB_NAME     = aws_rds_cluster.aurora.database_name
      DB_ENDPOINT = aws_rds_cluster.aurora.endpoint,
      DB_USERNAME = var.env,
      DB_PASSWORD = var.db_master_pass
      API_KEY     = var.api_key
    }
  }
}

resource "aws_iam_role" "lambda-exec" {
  name = "${var.service_name}-${var.env}-lambda-exec-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

}

resource "aws_lambda_permission" "lambda-permission" {
  statement_id  = "AllowMyDemoAPIInvoke"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.express.function_name
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_api_gateway_rest_api.rest-api.execution_arn}/*/*/*"
}

resource "aws_cloudwatch_log_group" "lambda" {
  name              = "/aws/lambda/${var.service_name}-${var.env}-fn"
  retention_in_days = 7
}

resource "aws_iam_policy" "lambda-policy" {
  name = "lambda_policy"
  path = "/"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "ec2:DescribeNetworkInterfaces",
        "ec2:CreateNetworkInterface",
        "ec2:DeleteNetworkInterface",
        "ec2:DescribeInstances",
        "ec2:AttachNetworkInterface"
      ],
      "Resource": "*"
    },
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*",
      "Effect": "Allow"
    },
    {
        "Effect": "Allow",
        "Action": [
            "rds-data:*"
        ],
        "Resource": [
            "*"
        ]
    },
    {
        "Effect": "Allow",
        "Action": [
            "secretsmanager:GetSecretValue"
        ],
        "Resource": [
            "${aws_secretsmanager_secret.pass.arn}"
        ]
    }
  ]
}
EOF
}

data "aws_iam_policy" "AWSLambdaVPCAccessExecutionRole" {
  arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
}

resource "aws_iam_role_policy_attachment" "lambda-logs" {
  role       = aws_iam_role.lambda-exec.name
  policy_arn = aws_iam_policy.lambda-policy.arn
}

resource "aws_iam_role_policy_attachment" "vpc-access" {
  role       = aws_iam_role.lambda-exec.name
  policy_arn = data.aws_iam_policy.AWSLambdaVPCAccessExecutionRole.arn
}

