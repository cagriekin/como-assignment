variable "service_name" {
  type    = string
  default = "como-demo"
}

variable "env" {
  type    = string
  default = "dev"
}

variable "db_master_pass" {
  type = string
}

variable "tld" {
  type = string
}

variable "api_key" {
  type = string
}
