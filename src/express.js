const express = require('express');
const bodyParser = require('body-parser');

const knexSetup = require('./lib/knex');
const { pino, loggerMiddleware } = require('./lib/logger');
const routeNotFound = require('./lib/route-not-found');
const version = require('./endpoints/version');
const handleErrors = require('./lib/error-handler');
const setupRequestValidator = require('./lib/request-validator');
const customerModel = require('./lib/models/customer');
const apiKeyValidator = require('./lib/api-key-validator');

const customerCreate = require('./endpoints/customer/create');
const customerGet = require('./endpoints/customer/get');
const customerUpdate = require('./endpoints/customer/update');
const customerList = require('./endpoints/customer/list');
const customerDelete = require('./endpoints/customer/delete');

const createApp = async () => {
  const { Router } = express;

  const knex = await knexSetup(pino);
  const Customer = customerModel(knex);
  const validator = await setupRequestValidator('./openapi.yml');

  const app = express();
  app.disable('x-powered-by');
  app.use(bodyParser.json());
  app.locals.Customer = Customer;
  app.use(loggerMiddleware);

  app.get('/version', version);

  const customerRouter = Router();
  customerRouter.use(apiKeyValidator);
  customerRouter.post('/', validator('customerCreate'), customerCreate);
  customerRouter.get('/:id', validator('customerGet'), customerGet);
  customerRouter.put('/:id', validator('customerPut'), customerUpdate);
  customerRouter.get('/', validator('customerList'), customerList);
  customerRouter.delete('/:id', validator('customerDelete'), customerDelete);
  app.use('/customer', customerRouter);

  app.use(routeNotFound);
  app.use(handleErrors);

  return app;
};

module.exports = createApp;
