const CustomError = require('./error');

/**
 * @param {Express.Request} req
 * @param {Express.Response} res
 * @param {Function} next
 */
const validator = (req, res, next) => {
  const { log } = req;
  const apiKey = process.env.API_KEY;

  if (!apiKey) {
    log.warn('No api-key provided, skipping check.');
    return next();
  }

  const gotHeader = req.get('x-api-key');
  if (!gotHeader) {
    log.info('x-api-key header not present');
    return next(CustomError.forbidden());
  }

  if (gotHeader !== apiKey) {
    log.info('x-api-key invalid value');
    return next(CustomError.forbidden());
  }

  next();
};

module.exports = validator;
