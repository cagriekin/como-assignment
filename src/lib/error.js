class CustomError extends Error {
  /**
   * @param {?object} err
   * @param {?string} err.message
   * @param {?number} err.statusCode
   */
  constructor({ message, statusCode = 500 } = {}) {
    super(message);
    this.statusCode = statusCode;
  }

  /**
   * @param {?object} err
   * @param {?string} err.message
   * @param {?number} err.statusCode
   */
  static notFound({ message = 'Not found', statusCode = 404 } = {}) {
    return new CustomError({
      message,
      statusCode
    });
  }

  /**
   * @param {?object} err
   * @param {?string} err.message
   * @param {?number} err.statusCode
   */
  static badRequest({ message = 'Bad Request', statusCode = 400 } = {}) {
    return new CustomError({
      message,
      statusCode
    });
  }

  /**
   * @param {?object} err
   * @param {?string} err.message
   * @param {?number} err.statusCode
   */
  static forbidden({ message = 'Forbidden', statusCode = 403 } = {}) {
    return new CustomError({
      message,
      statusCode
    });
  }

  /**
   * @param {?object} err
   * @param {?string} err.message
   * @param {?number} err.statusCode
   */
  static badGateway({ message = 'Bad Gateway', statusCode = 502 } = {}) {
    return new CustomError({
      message,
      statusCode
    });
  }

  /**
   * @param {?object} err
   * @param {?string} err.message
   * @param {?number} err.statusCode
   */
  static internalError({ message = 'Internal Error', statusCode = 500 } = {}) {
    return new CustomError({
      message,
      statusCode
    });
  }
}

module.exports = CustomError;
