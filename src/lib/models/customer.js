const { Model } = require('objection');

const aYearInNanoSeconds = 31536000000;

module.exports = knex => {
  Model.knex(knex);

  class Customer extends Model {
    static get tableName() {
      return 'customers';
    }

    get age() {
      return Math.floor((Date.now() - Date.parse(this.birthDate)) / aYearInNanoSeconds);
    }

    toJSON() {
      return {
        id: this.id,
        firstName: this.firstName,
        lastName: this.lastName,
        age: this.age
      };
    }
  }

  return Customer;
};
