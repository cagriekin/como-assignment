/* eslint-disable no-unused-vars */
/**
 *
 * @param {Error} err
 * @param {Express.Request} req
 * @param {Express.Response} res
 * @param {Express.NextFunction} next
 */
module.exports = (err, req, res, next) => {
  const { log } = req;

  log.error(err);
  res.status(err.statusCode || 500).send({ message: err.message });
};
