/* eslint-disable no-unused-vars, consistent-return */
const SwaggerParser = require('@apidevtools/swagger-parser');
const Enjoi = require('enjoi');
const CustomError = require('./error');
const { pino: logger } = require('./logger');

/**
 * @param {string} filePath
 * @returns {Promise}
 */
const initValidator = async filePath => {
  let api;
  try {
    api = await SwaggerParser.validate(filePath);
  } catch (err) {
    logger.fatal(err);
    process.exitCode = 1;
  }

  /**
   * opId {string} operationId defined in openapi spec
   */
  return opId => {
    const pathDef = Object.values(api.paths)
      .map(Object.values)
      .flat()
      .find(({ operationId }) => operationId === opId);

    if (!pathDef) throw CustomError.notFound({ message: 'OperationId not defined in openapi spec' });

    return async (req, res, next) => {
      if (!pathDef.requestBody) return next();

      const contentType = req.get('content-type') || 'application/json';
      if (!Object.keys(pathDef.requestBody.content).includes(contentType))
        return next(CustomError.badRequest({ message: `${contentType} isn't available` }));

      const bodySchema = Enjoi.schema(pathDef.requestBody.content[contentType].schema);

      try {
        await bodySchema.validateAsync(req.body);
      } catch (err) {
        return next(CustomError.badRequest(err));
      }

      next();
    };
  };
};

module.exports = initValidator;
