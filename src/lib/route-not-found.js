const CustomError = require('./error');

module.exports = (req, res, next) => next(CustomError.notFound());
