/* eslint-disable global-require,import/no-extraneous-dependencies */
const { v4: genCorrId } = require('uuid');

const opts =
  process.env.NODE_ENV !== 'production'
    ? {
        prettyPrint: {
          levelFirst: true
        },
        prettifier: require('pino-pretty')
      }
    : {};

const pino = require('pino')(opts);

/**
 * @param {Express.Request} req
 * @param {Express.Response} res
 * @param {Function} next
 */
const loggerMiddleware = (req, res, next) => {
  const { method, path, params, query, body } = req;
  const correlationId = genCorrId();

  const logger = pino.child({ correlationId });
  logger.info({ msg: 'Incoming request', method, path, params, query, body });

  req.log = logger;
  res.set('x-correlation-id', correlationId);

  next();
};

module.exports = { pino, loggerMiddleware };
