const Knex = require('knex');

module.exports = async logger => {
  let conn;

  try {
    if (process.env.NODE_ENV !== 'production')
      conn = await Knex({
        client: 'sqlite3',
        useNullAsDefault: true,
        connection: {
          filename: 'customers.db'
        }
      });
    else
      conn = await Knex({
        client: 'mysql',
        connection: {
          host: process.env.DB_ENDPOINT,
          user: process.env.DB_USERNAME,
          password: process.env.DB_PASSWORD,
          database: process.env.DB_NAME
        },
        acquireConnectionTimeout: 5000
      });
  } catch (err) {
    logger.error(err, 'Knex init error');
    process.exitCode = 1;
  }

  try {
    if (await conn.schema.hasTable('customers')) return conn;
  } catch (err) {
    logger.error(err, 'Knex schema check failed');
    process.exitCode = 1;
  }

  try {
    // Not bothering with knex migration files as this is only an assignment.
    logger.info('DB table doesnt exists, creating one');

    await conn.schema.createTable('customers', table => {
      table.uuid('id').primary();
      table.string('firstName');
      table.string('lastName');
      table.date('birthDate');
    });

    logger.info('Created db table');
  } catch (err) {
    logger.fatal(err, 'Failed to create customers table');
    process.exitCode = 1;
  }

  return conn;
};
