const http = require('http');
const { pino: logger } = require('./lib/logger');
const expressApp = require('./express');

(async () => {
  const server = http.createServer(await expressApp());

  server
    .listen(process.env.PORT || '8000')
    .on('listening', () => {
      const { address, port } = server.address();
      logger.info('Listening on %s:%s', address, port);
    })
    .on('error', error => {
      logger.fatal(error, 'Cannot start http server');
      throw error;
    });
})();
