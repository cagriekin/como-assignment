const asyncHandler = require('express-async-handler');

const deleteFn = async (req, res) => {
  const {
    app: {
      locals: { Customer }
    },
    params: { id },
    log
  } = req;

  res.status(202).end();

  try {
    await Customer.query().deleteById(id);
  } catch (err) {
    log.error(err);
  }
};

module.exports = asyncHandler(deleteFn);
