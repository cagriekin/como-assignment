const asyncHandler = require('express-async-handler');
const CustomError = require('../../lib/error');

const get = async (req, res) => {
  const {
    app: {
      locals: { Customer }
    },
    params: { id },
    log
  } = req;

  let customer;
  try {
    customer = await Customer.query().findOne({ id });
  } catch (err) {
    log.error(err);
    throw CustomError.internalError();
  }

  if (!customer) throw CustomError.notFound();

  res.status(200).send(customer);
};

module.exports = asyncHandler(get);
