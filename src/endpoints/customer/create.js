const { v4: uuidv4 } = require('uuid');
const asyncHandler = require('express-async-handler');
const got = require('got');
const CustomError = require('../../lib/error');

const create = async (req, res) => {
  const {
    body: { firstName, lastName, birthDate },
    app: {
      locals: { Customer }
    },
    log
  } = req;

  const savePromise = Customer.query()
    .insertAndFetch({ id: uuidv4(), firstName, lastName, birthDate })
    .then(c => Customer.fromJson(c).toJSON())
    .catch(err => {
      log.error(err);
      throw CustomError.internalError();
    });
  const jokePromise = got(`http://api.icndb.com/jokes/random?firstName=${firstName}&lastName=${lastName}`, {
    responseType: 'json',
    timeout: 3000
  })
    .then(
      ({
        body: {
          value: { joke }
        }
      }) => joke
    )
    .catch(err => {
      log.error(err);
      throw CustomError.badGateway();
    });

  const [customer, joke] = await Promise.all([savePromise, jokePromise]);

  res.status(201).send({ ...customer, joke });
};

module.exports = asyncHandler(create);
