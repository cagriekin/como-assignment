const asyncHandler = require('express-async-handler');
const CustomError = require('../../lib/error');

const update = async (req, res) => {
  const {
    body: { firstName, lastName, birthDate },
    app: {
      locals: { Customer }
    },
    params: { id },
    log
  } = req;

  let affectedRows;
  try {
    affectedRows = await Customer.query().update({ firstName, lastName, birthDate }).where({ id });
  } catch (err) {
    log.error(err);
    throw CustomError.internalError();
  }

  if (!affectedRows || affectedRows < 1) throw CustomError.notFound();

  res.status(204).end();
};

module.exports = asyncHandler(update);
