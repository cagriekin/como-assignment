const asyncHandler = require('express-async-handler');
const CustomError = require('../../lib/error');

const list = async (req, res) => {
  const {
    app: {
      locals: { Customer }
    },
    query: { order, dir, page, limit },
    log
  } = req;

  let customers;

  try {
    customers = await Customer.query()
      .orderBy(order || 'firstName', dir || 'asc')
      .offset(Math.floor(((page || 1) - 1) * (limit || 100)))
      .limit(limit || 100);
  } catch (err) {
    log.error(err);
    throw CustomError.internalError();
  }

  res.status(200).send(customers);
};

module.exports = asyncHandler(list);
