const pkg = require('../../package.json');

module.exports = (_, res) =>
  res.status(200).send({
    name: pkg.name,
    version: pkg.version
  });
