const serverlessExpress = require('@vendia/serverless-express');
const app = require('./express');

let server;
exports.handler = async (event, context) => {
  context.callbackWaitsForEmptyEventLoop = false;
  if (!server) server = serverlessExpress.createServer(await app());

  return serverlessExpress.proxy(server, event, context, 'PROMISE').promise;
};
