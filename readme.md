# CoMo Assignment

## Running and Testing Locally

### Running
```
npm i
npm run dev
```

### Unit Testing
```
npm i
npm test
```

### API Testing

Get your `restbeast` build from  [https://github.com/restbeast/restbeast/releases/tag/v0.12.4](https://github.com/restbeast/restbeast/releases/tag/v0.12.4)
Linux amd64 example:
```
tar zxvf restbeast-v0.12.4-linux-amd64.tar.gz
sudo mv restbeast /usr/local/bin/
```

Switch to API requests folder
```
cd test/api
restbeast request create # Does a POST /customer
restbeast test create # Runs create tests

restbeast request get # Does a GET /customer/:id
restbeast test get # Runs get tests

restbeast request list # Does a GET /customer
restbeast request list-order-lastname-desc # Ordered by `lastName` DESC with 10 results per page, only the first page.
restbeast test list # Runs list tests

restbeast request delete # Does a DELwETE /customer/:id
restbeast test delete # Runs delete tests

restbeast request update # Does a PUT /customer/:id
restbeast test update # Runs update tests

restbeast test # Runs all tests
```

## Testing Dev Env.

Get your `restbeast` build from [https://github.com/restbeast/restbeast/releases/tag/v0.12.4](https://github.com/restbeast/restbeast/releases/tag/v0.12.4)
Linux amd64 example:
```
tar zxvf restbeast-v0.12.4-linux-amd64.tar.gz
sudo mv restbeast /usr/local/bin/
```

Switch to API requests folder
```
cd test/api
export restbeast_var_APIKEY=open-sesame # Set the api-key

restbeast request create --env dev # Does a POST /customer
restbeast test create --env dev # Runs create tests
.
.
.
restbeast attack-request create --env dev -c 60 -p 60s # Runs a load test on POST /customer by sending 60 requests in 60 seconds. 
```

## Deploying to dev env.

Assuming `terraform` 0.14 or newer installed, and a top level domain is hosted in aws route53, and AWS credentials are properly set. 

```
npm i
npm run lambda-build
cd terraform/dev
export TF_VAR_db_master_pass=PICK_A_PASS
export TF_VAR_tld=A_TOP_LEVEL_DOMAIN
export TF_VAR_api_key=open-sesame
terraform plan -out plan.tfout
terraform apply plan.tfout
```

## Running with docker
```
docker build .
run -p 8000:8000 contanerId
```

## TODO
Things I'd like to do but decided against it because of timing and scope reasons. 

- I originally planed to have a prod env. terraform scripts that would use
  - ECS or EKS instead of lambda.
  - Aurora multi-master db instead of aurora serverless 
  - WAF rules
  - Handle API key in API gateway with rate limiter 
- Have github actions to deploy on push to dev and on tag to prod 
