FROM node:12-alpine
WORKDIR /usr/src/app

COPY package*.json ./
RUN npm install --only=production --no-optional
COPY . .

EXPOSE 8000
ENV NODE_ENV=production
CMD [ "node", "src/boot.js" ]
